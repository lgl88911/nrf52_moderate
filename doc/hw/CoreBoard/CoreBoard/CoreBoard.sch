EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:nrf52cb
LIBS:CoreBoard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L nrf52CB U1
U 1 1 5B50962F
P 4950 2750
F 0 "U1" H 4950 2850 60  0000 C CNN
F 1 "nrf52CB" H 4950 2550 60  0000 C CNN
F 2 "" H 4950 2750 60  0001 C CNN
F 3 "" H 4950 2750 60  0001 C CNN
	1    4950 2750
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x05_Counter_Clockwise J1
U 1 1 5B5097B1
P 3450 3100
F 0 "J1" H 3500 3400 50  0000 C CNN
F 1 "Conn_02x05_Counter_Clockwise" H 3500 2800 50  0001 C CNN
F 2 "" H 3450 3100 50  0001 C CNN
F 3 "" H 3450 3100 50  0001 C CNN
	1    3450 3100
	1    0    0    -1  
$EndComp
Text Label 3850 3300 2    40   ~ 0
RST
Text Label 3000 3100 0    40   ~ 0
TXD
Text Label 3000 3000 0    40   ~ 0
RXD
$Comp
L +3.3V #PWR?
U 1 1 5B509959
P 4050 2850
F 0 "#PWR?" H 4050 2700 50  0001 C CNN
F 1 "+3.3V" H 4050 2990 50  0000 C CNN
F 2 "" H 4050 2850 50  0001 C CNN
F 3 "" H 4050 2850 50  0001 C CNN
	1    4050 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3000 3000 3000
Wire Wire Line
	3250 3100 3000 3100
Wire Wire Line
	3750 2900 4050 2900
Wire Wire Line
	3750 3300 3850 3300
Wire Wire Line
	3750 3000 3950 3000
Wire Wire Line
	3750 3100 3950 3100
Wire Wire Line
	3750 3200 4050 3200
Wire Wire Line
	4050 3200 4050 3250
$Comp
L GND #PWR?
U 1 1 5B5098BE
P 4050 3250
F 0 "#PWR?" H 4050 3000 50  0001 C CNN
F 1 "GND" H 4050 3100 50  0000 C CNN
F 2 "" H 4050 3250 50  0001 C CNN
F 3 "" H 4050 3250 50  0001 C CNN
	1    4050 3250
	1    0    0    -1  
$EndComp
Text Label 3950 3000 2    40   ~ 0
SWDIO
Text Label 3950 3100 2    40   ~ 0
SWCLK
Wire Wire Line
	5400 2550 5800 2550
Wire Wire Line
	5400 2600 5800 2600
Wire Wire Line
	5400 2650 5800 2650
Wire Wire Line
	4500 3100 4250 3100
Wire Wire Line
	4500 2550 4250 2550
Wire Wire Line
	4500 2600 4250 2600
Wire Wire Line
	5150 3400 5150 3500
Wire Wire Line
	4050 2900 4050 2850
NoConn ~ 3250 3300
NoConn ~ 3250 3200
NoConn ~ 3250 2900
$Comp
L +3.3V #PWR?
U 1 1 5B509BEC
P 4250 3050
F 0 "#PWR?" H 4250 2900 50  0001 C CNN
F 1 "+3.3V" H 4250 3190 50  0000 C CNN
F 2 "" H 4250 3050 50  0001 C CNN
F 3 "" H 4250 3050 50  0001 C CNN
	1    4250 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 3100 4250 3050
Text Label 4250 2550 0    25   ~ 5
RXD
Text Label 4250 2600 0    25   ~ 5
TXD
Text Label 5800 2550 2    25   ~ 5
SWDIO
Text Label 5800 2600 2    25   ~ 5
SWCLK
Text Label 5800 2650 2    25   ~ 5
RST
$Comp
L GND #PWR?
U 1 1 5B509D17
P 5150 3500
F 0 "#PWR?" H 5150 3250 50  0001 C CNN
F 1 "GND" H 5150 3350 50  0000 C CNN
F 2 "" H 5150 3500 50  0001 C CNN
F 3 "" H 5150 3500 50  0001 C CNN
	1    5150 3500
	1    0    0    -1  
$EndComp
Text Notes 3900 3500 2    60   ~ 0
DAPLink Interface
Text Notes 5150 2350 2    60   ~ 0
CoreBoard
$EndSCHEMATC
