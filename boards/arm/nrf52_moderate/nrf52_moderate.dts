/*
 * Copyright (c) 2018 Frank Limited
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/dts-v1/;
#include <nordic/nrf52832_qfaa.dtsi>

/ {
	model = "Frank nrf52 moderate";
	compatible = "Frank", "nordic,nrf52832-qfaa",
		     "nordic,nrf52832";

	chosen {
		zephyr,console = &uart0;
		zephyr,uart-mcumgr = &uart0;
		zephyr,shell-uart = &uart0;
		zephyr,sram = &sram0;
		zephyr,flash = &flash0;
		zephyr,code-partition = &slot0_partition;
	};
	dht11 {
		compatible = "aosong,dht";
		status = "okay";
		label = "DHT11";
		dio-gpios = <&gpio0 14 0>;
	};
};

&gpiote {
	status ="okay";
};

&gpio0 {
	status ="okay";
};

&uart0 {
	compatible = "nordic,nrf-uart";
	current-speed = <115200>;
	status = "okay";
	tx-pin = <25>;
	rx-pin = <26>;
};

&spi1 {
	status = "okay";
	sck-pin = <11>;
	mosi-pin = <6>;
	miso-pin = <12>;
};

&adc {
	status ="okay";
};

&flash0 {
	/*
	 * For more information, see:
	 * http://docs.zephyrproject.org/latest/devices/dts/flash_partitions.html
	 */
	partitions {
		compatible = "fixed-partitions";
		#address-cells = <1>;
		#size-cells = <1>;

		boot_partition: partition@0 {
			label = "mcuboot";
			reg = <0x00000000 0xc000>;
		};
		slot0_partition: partition@c000 {
			label = "image-0";
			reg = <0x0000C000 0x32000>;
		};
		slot1_partition: partition@3e000 {
			label = "image-1";
			reg = <0x0003E000 0x32000>;
		};
		scratch_partition: partition@70000 {
			label = "image-scratch";
			reg = <0x00070000 0xa000>;
		};
		storage_partition: partition@7a000 {
			label = "storage";
			reg = <0x0007a000 0x00006000>;
		};
	};
};
