/*
 * Copyright (c) 2018 Frank Limited.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __INC_BOARD_H
#define __INC_BOARD_H

#include <soc.h>

#define LED_GPIO_PORT CONFIG_GPIO_P0_DEV_NAME
#define LED_RED_PIN 20
#define LED_BLUE_PIN 19
#define LED_GREEN_PIN 18

#define LED_NUMBER  3

#define PWM_DRIVER CONFIG_PWM_NRF5_SW_0_DEV_NAME


#endif /* __INC_BOARD_H */
