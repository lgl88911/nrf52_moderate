board_runner_args(pyocd "--target=nrf52")
board_runner_args(pyocd "--flash-opt=-f 2000000")
include(${ZEPHYR_BASE}/boards/common/pyocd.board.cmake)
