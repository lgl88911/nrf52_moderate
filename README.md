# nrf52_moderate

#### 项目介绍
nrf52 moderate is a diy board, which run zephyr

#### 软件架构


#### 安装教程



#### 使用说明
```
git clone https://gitee.com/lgl88911/nrf52_moderate.git ~/work/project/nrf52_moderate 
cd ~/work/project/nrf52_moderate/apps/basetest 
source ~/work/project/nrf52_moderate/script/arm.sh 
source ~/work/project/zephyr/zephyr-env.sh 
mkdir build 
cd build 
cmake -GNinja -DBOARD=nrf52_mother -DBOARD_ROOT=/home/frank/work/project/nrf52_moderate .. 
ninja 
```
Connext DAPLink to nrf52_moderate, do shell cmd
```
ninja flash 
```
or 
```
ninja debug 
```

##### Debug by VSCode 
Open VSCode  
Open ~/work/project/nrf52_moderate/ in VSCode workspace  
Click Debug Button  
Select Debug Microcontroller(nrf52_moderate) and run  


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request



