#include "lvgl.h"

/*******************************************************************************
 * Size: 16 px
 * Bpp: 1
 * Opts: 
 ******************************************************************************/

#ifndef HANG
#define HANG 1
#endif

#if HANG

/*-----------------
 *    BITMAPS
 *----------------*/

/*Store the image of the glyphs*/
static LV_ATTRIBUTE_LARGE_CONST const uint8_t gylph_bitmap[] = {
    /* U+4E0B "下" */
    0x0, 0x3, 0xff, 0xf0, 0x20, 0x0, 0x80, 0x2,
    0x0, 0xe, 0x0, 0x26, 0x0, 0x8e, 0x2, 0x8,
    0x8, 0x0, 0x20, 0x0, 0x80, 0x2, 0x0, 0x8,
    0x0, 0x0, 0x0,

    /* U+4E2D "中" */
    0x0, 0x0, 0xc, 0x0, 0x30, 0x0, 0xc0, 0xff,
    0xfb, 0xc, 0x2c, 0x30, 0xb0, 0xc2, 0xc3, 0xb,
    0xff, 0xec, 0x30, 0x80, 0xc0, 0x3, 0x0, 0xc,
    0x0, 0x30, 0x0, 0x0,

    /* U+8F7D "载" */
    0x0, 0x0, 0x10, 0xa1, 0xfd, 0x20, 0x42, 0xf,
    0xff, 0xc2, 0x8, 0xc, 0x12, 0x7f, 0xa4, 0x20,
    0x58, 0xd8, 0xe1, 0xfc, 0xc0, 0x61, 0x3, 0xf6,
    0x41, 0x9a, 0x83, 0x67, 0x0, 0x0
};


/*---------------------
 *  GLYPH DESCRIPTION
 *--------------------*/

static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
    {.bitmap_index = 0, .adv_w = 0, .box_h = 0, .box_w = 0, .ofs_x = 0, .ofs_y = 0} /* id = 0 reserved */,
    {.bitmap_index = 0, .adv_w = 256, .box_h = 15, .box_w = 14, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 27, .adv_w = 256, .box_h = 16, .box_w = 14, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 55, .adv_w = 256, .box_h = 16, .box_w = 15, .ofs_x = 1, .ofs_y = -2}
};

/*---------------------
 *  CHARACTER MAPPING
 *--------------------*/

static const uint16_t unicode_list_0[] = {
    0x0, 0x22, 0x4172
};

/*Collect the unicode lists and glyph_id offsets*/
static const lv_font_fmt_txt_cmap_t cmaps[] =
{
    {
        .range_start = 19979, .range_length = 16755, .type = LV_FONT_FMT_TXT_CMAP_SPARSE_TINY,
        .glyph_id_start = 1, .unicode_list = unicode_list_0, .glyph_id_ofs_list = NULL, .list_length = 3
    }
};



/*--------------------
 *  ALL CUSTOM DATA
 *--------------------*/

/*Store all the custom data of the font*/
static lv_font_fmt_txt_dsc_t font_dsc = {
    .glyph_bitmap = gylph_bitmap,
    .glyph_dsc = glyph_dsc,
    .cmaps = cmaps,
    .cmap_num = 1,
    .bpp = 1,

    .kern_scale = 0,
    .kern_dsc = NULL,
    .kern_classes = 0,
};


/*-----------------
 *  PUBLIC FONT
 *----------------*/

/*Initialize a public general font descriptor*/
lv_font_t hang = {
    .dsc = &font_dsc,          /*The custom font data. Will be accessed by `get_glyph_bitmap/dsc` */
    .get_glyph_bitmap = lv_font_get_bitmap_fmt_txt,    /*Function pointer to get glyph's bitmap*/
    .get_glyph_dsc = lv_font_get_glyph_dsc_fmt_txt,    /*Function pointer to get glyph's data*/
    .line_height = 16,          /*The maximum line height required by the font*/
    .base_line = 2,             /*Baseline measured from the bottom of the line*/
};

#endif /*#if HANG*/

