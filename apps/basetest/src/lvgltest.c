/*
 * Copyright (c) 2018 PHYTEC Messtechnik GmbH
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <device.h>
#include <drivers/display.h>
#include <stdio.h>
#include <lvgl.h>

LV_FONT_DECLARE(hang)        /*Declare a font*/

void lvgltest_init(void)
{
	u32_t count = 0U;
	char count_str[11] = {0};
	struct device *display_dev;
	lv_obj_t *hello_world_label;
	lv_obj_t *count_label;
	lv_obj_t *title_label;

	display_dev = device_get_binding(CONFIG_LVGL_DISPLAY_DEV_NAME);

	if (display_dev == NULL) {
		printk("device not found.  Aborting test.");
		return;
	}

    // set mono theme - maybe this is the default for 1 bit/pixel anyway?
    //  hue doesn't seem to make a difference
    lv_theme_mono_init(0 /* hue */,NULL /* use LV_FONT_DEFAULT */);
    lv_theme_set_current( lv_theme_get_mono() );

	static lv_style_t style1;
    lv_style_copy(&style1, &lv_style_plain);
    style1.text.font = &hang; /*Set the base font whcih is concatenated with the others*/

	title_label  = lv_label_create(lv_scr_act(), NULL);
	lv_obj_set_pos(title_label, 40, 0);
	lv_label_set_style(title_label, LV_LABEL_STYLE_MAIN, &style1);
	lv_label_set_text(title_label, "下载中");             

	lv_obj_t * btn_pause = lv_btn_create(lv_scr_act(), NULL);    
	lv_obj_set_pos(btn_pause, 4, 48);
	lv_obj_set_size(btn_pause, 46, 12);             
	lv_obj_t * label_pause = lv_label_create(btn_pause, NULL);       
	lv_label_set_text(label_pause, "pause");             

	lv_obj_t * btn_cancel = lv_btn_create(lv_scr_act(), NULL);    
	lv_obj_set_pos(btn_cancel, 80, 48);
	lv_obj_set_size(btn_cancel, 46, 12);             
	lv_obj_t * label_cancel = lv_label_create(btn_cancel, NULL);       
	lv_label_set_text(label_cancel, "exit");            

	hello_world_label = lv_label_create(lv_scr_act(), NULL);
	lv_obj_set_pos(hello_world_label, 3, 27);
	lv_label_set_text(hello_world_label, "pc:");


	count_label = lv_label_create(lv_scr_act(), NULL);
	lv_obj_set_pos(count_label, 110, 27);

	lv_obj_t * bar1 = lv_bar_create(lv_scr_act(), NULL);
	lv_obj_set_pos(bar1, 26, 25);
    lv_obj_set_size(bar1, 80, 12);
    //lv_bar_set_anim_time(bar1, 5000);
    //lv_bar_set_value(bar1, 100, LV_ANIM_ON);

	display_blanking_off(display_dev);

	while (1) {
		if ((count % 100) == 0U) {
			if(count/100<100){
				sprintf(count_str, "%d", count/100U);
				lv_label_set_text(count_label, count_str);
				lv_bar_set_value(bar1, count/100U, LV_ANIM_OFF);
			}			
		}
		lv_task_handler();
		k_sleep(2);
		++count;
	}
}