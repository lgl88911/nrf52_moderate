#ifndef __TEST_H__
#define __TEST_H__

void bt_test_init(void);
void net_test_init(void);
void spinor_test_init();
void distest_init(void);
void distest_set(char * str);
void fstest_init(void);
void lvgltest_init(void);
void use_space_test(void);
#endif