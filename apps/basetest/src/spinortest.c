#include <zephyr/types.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <sys/util.h>
#include <flash.h>
#include <logging/log.h>

#define LOG_MODULE_NAME spi_nor_test
LOG_MODULE_REGISTER(LOG_MODULE_NAME, LOG_LEVEL_DBG);





void spinor_test_init()
{
	struct device *spinor = NULL;
    char data[32];

	//spinor = device_get_binding(DT_JEDEC_SPI_NOR_0_LABEL);
    LOG_ERR("Get device %p", spinor);
    if(spinor != NULL){
        flash_read(spinor,0,data,32);
        LOG_HEXDUMP_ERR(data, 32, "read");
        flash_erase(spinor, 0, 32);
        flash_read(spinor,0,data,32);
        LOG_HEXDUMP_ERR(data, 32, "erase");
    }
}