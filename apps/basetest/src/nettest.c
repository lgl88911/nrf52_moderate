/*
 * Copyright (c) 2017 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <stdio.h>
#include <net/net_ip.h>
#include <net/ethernet.h>
#include <logging/log.h>

#define LOG_MODULE_NAME net_test
LOG_MODULE_REGISTER(LOG_MODULE_NAME, LOG_LEVEL_DBG);





static int bt_net_init()
{
	struct in6_addr ipv6_target;
	struct device *dev;
	struct net_if *iface;

	dev = device_get_binding("net_bt");
	if (!dev) {
		LOG_ERR("No BLE device interface found.");
	}

	iface = net_if_lookup_by_dev(dev);
	if (!iface) {
		LOG_ERR("No BLE network interface found.");
	}

	if (net_addr_pton(AF_INET6, "2002:db8::6", &ipv6_target) < 0) {
		LOG_ERR("covert ip error.");
		return -EINVAL;
	}

	net_if_ipv6_addr_add(iface, &ipv6_target,
			     NET_ADDR_MANUAL, 0);
	
	//net_route_add(iface, &ipv6_target, 64, &ipv6_target);

	return 0;
}


void net_test_init(void)
{
    struct net_if *iface;
    
    iface = net_if_get_default();
    ethernet_init(iface);
	//bt_net_init();
}

