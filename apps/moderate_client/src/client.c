/* echo-client.c - Networking echo client */

/*
 * Copyright (c) 2017 Intel Corporation.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/*
 * The echo-client application is acting as a client that is run in Zephyr OS,
 * and echo-server is run in the host acting as a server. The client will send
 * either unicast or multicast packets to the server which will reply the packet
 * back to the originator.
 *
 * In this sample application we create four threads that start to send data.
 * This might not be what you want to do in your app so caveat emptor.
 */

#include <logging/log.h>
LOG_MODULE_REGISTER(net_echo_client_sample, LOG_LEVEL_DBG);

#include <zephyr.h>
#include <errno.h>
#include <stdio.h>

#include <net/net_pkt.h>
#include <net/net_core.h>
#include <net/net_context.h>

void bt_net_init()
{
	struct in6_addr ipv6_target;
	struct device *dev;
	struct net_if *iface;

	dev = device_get_binding("net_bt");
	if (!dev) {
		LOG_ERR("No BLE device interface found.");
	}

	iface = net_if_lookup_by_dev(dev);
	if (!iface) {
		LOG_ERR("No BLE network interface found.");
	}

	if (net_addr_pton(AF_INET6, "2002:db8::10", &ipv6_target) < 0) {
		LOG_ERR("covert ip error.");
		return -EINVAL;
	}

	net_if_ipv6_addr_add(iface, &ipv6_target,
			     NET_ADDR_MANUAL, 0);
}

void main(void)
{
	int ret;
	LOG_ERR("bt net init");
	bt_net_init();
}
